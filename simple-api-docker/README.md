## Buiding this thing locally

```shell
$> docker build --tag simple-api:latest .
```

## Running this thing locally

```shell
$> docker run --rm -p 8080:8080 simple-api:latest
```

---

# Deploy to Openshift

```shell
$> oc login -u developer https://api.crc.testing:6443

$> oc new-project simple-api
```

```shell
$> oc new-app https://gitlab.com/mt-data-group/k8s-simple-api \
    --context-dir=simple-api-docker \
    --strategy=docker
```

### Output should be similar to below:

```text
--> Found container image 81dbc1f (3 days old) from Docker Hub for "python:3.9"

    * An image stream tag will be created as "python:3.9" that will track the source image
    * A Docker build using source code from https://gitlab.com/mt-data-group/k8s-simple-api will be created
      * The resulting image will be pushed to image stream tag "k8s-simple-api:latest"
      * Every time "python:3.9" changes a new build will be triggered

--> Creating resources ...
    imagestream.image.openshift.io "python" created
    imagestream.image.openshift.io "k8s-simple-api" created
    buildconfig.build.openshift.io "k8s-simple-api" created
    deployment.apps "k8s-simple-api" created
    service "k8s-simple-api" created
--> Success
    Build scheduled, use 'oc logs -f buildconfig/k8s-simple-api' to track its progress.
    Application is not exposed. You can expose services to the outside world by executing one or more of the commands below:
     'oc expose service/k8s-simple-api' 
    Run 'oc status' to view your app.

```

## Expose the service
```shell

$> # expose the service
$> oc expose service/k8s-simple-api --port=8080

$> # get the route
$> oc get routes

$> oc get route k8s-simple-api
```

---

## make a change -> checkin -> rebuild
```shell
$> # get all build configs
$> oc get bc

$> # kick-off a new build
$> oc start-build bc/k8s-simple-api

$> # stream logs 
$> oc logs -f buildconfig/k8s-simple-api
```

## Cleanup
```shell
$> oc get project --show-labels

$> oc get all -l app=k8s-simple-api


$> oc delete all,is,dc,secrets,cm,pvc -l app=k8s-simple-api
```

# Deploy to K8s

## Push to Container Registry (DockerHub)

```shell
$> minikube start

$> alias kubectl="minikube kubectl --"

$> minikube dashboard

```

```shell
$> docker login 

$> docker tag simple-api:latest mastella/simple-api:latest

$> docker push mastella/simple-api:latest

$> docker tag mpstella/simple-api:1.0.0

```

## check context and create namespace
```shell
$> kubectl config view

$> # using docker desktop locally
$> kubectl config use-context minikube

$> # list the namespaces
$> kubectl get namespaces

$> # create a demo namespace
$> kubectl create namespace demo
```

## Deploy
```shell
$> kubectl apply -f k8s/deployment.yaml -n demo

$> # check logs ..
$> kubectl logs --selector app=simple-api -n demo

$> # deploy the NodePort, or .....
$> kubectl apply -f k8s/svc-np.yaml -n demo

$> # deploy the LoadBalancer
$> kubectl apply -f k8s/svc-lb.yaml -n demo

$> # check the deployments
$> kubectl get deployments -n demo

$> # check the pods
$> kubectl get pods -n demo

```


## Check the service is up

```shell
$> kubectl get svc -n demo

$> # either port forward, or ...
$> kubectl port-forward svc/simple-api-svc 8080:80 -n demo

$> # or ask minikube to sort it out
$> minikube service simple-api-svc -n demo

```

Output would look like below:

```text
NAME             TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)        AGE
kubernetes       ClusterIP   10.96.0.1      <none>        443/TCP        23h
simple-api-svc   NodePort    10.102.18.89   <none>        80:30100/TCP   3s
```

goto [http://127.0.0.1:30100/](http://127.0.0.1:30100/) and fingers crossed

## (Optional) deploy Horizontal Auto Scaler
```shell
$> kubectl apply -f k8s/autoscale.yaml
```

## Cleanup
```shell

$> # full blown delete
$> kubectl delete all --all -n demo

$> # indivual delete
$> kubectl delete deployment simple-api -n demo
$> kubectl delete svc simple-api-svc -n demo
$> kubectl delete hpa simple-api-hpa -n demo
```