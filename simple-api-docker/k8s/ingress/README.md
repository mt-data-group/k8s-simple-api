## Ingress (minikube)

```sh
$> # enable in minikube

$> minikube addons enable ingress

$> # install via helm

$> helm upgrade --install ingress-nginx ingress-nginx \
  --repo https://kubernetes.github.io/ingress-nginx \
  --namespace ingress-nginx --create-namespace

$> # install via YAML

$> kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.7.0/deploy/static/provider/cloud/deploy.yaml

$> # check

$> kubectl get pods --all-namespaces -l app=ingress-nginx

```

## Ingress (kd3)

```sh
$> kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.7.0/deploy/static/provider/cloud/deploy.yaml

$> k3d cluster create test-ing --agents 2 --port '8080:80@loadbalancer'
```

## Deploy Apple & Banana

```sh

$> kubectl create ns app-ns-testing

$> kubectl apply -f apple.yaml -n app-ns-testing
$> kubectl apply -f banana.yaml -n app-ns-testing

$> kubectl apply -f ingress.yaml -n app-ns-testing
```

## Test

```sh
$> curl kubernetes.docker.internal:8080/apple
apple

$> curl kubernetes.docker.internal:8080/banana
banana
```