[source-to-image](https://github.com/openshift/source-to-image)

## Getting started
```shell
$> s2i create simple-api-s2i simple-api-s2i
```


```shell
$> oc new-app python:3.9~https://gitlab.com/mt-data-group/k8s-simple-api \
    --context-dir=simple-api-s2i-custom \
    --strategy=source

```

```text
warning: Cannot check if git requires authentication.
--> Found container image 81dbc1f (3 days old) from Docker Hub for "python:3.9"

    * An image stream tag will be created as "python:3.9" that will track the source image
    * A source build using source code from https://gitlab.com/mt-data-group/k8s-simple-api will be created
      * The resulting image will be pushed to image stream tag "k8s-simple-api:latest"
      * Every time "python:3.9" changes a new build will be triggered

--> Creating resources ...
    imagestream.image.openshift.io "python" created
    imagestream.image.openshift.io "k8s-simple-api" created
    buildconfig.build.openshift.io "k8s-simple-api" created
    deployment.apps "k8s-simple-api" created
--> Success
    Build scheduled, use 'oc logs -f buildconfig/k8s-simple-api' to track its progress.
    Run 'oc status' to view your app.

```

## Expose the service
```shell

$> # expose the service
$> oc expose service/k8s-simple-api --port=8080

$> # get the route
$> oc get routes

$> oc get route k8s-simple-api
```

---

## make a change -> checkin -> rebuild
```shell
$> # get all build configs
$> oc get bc

$> # kick-off a new build
$> oc start-build bc/k8s-simple-api

$> # stream logs 
$> oc logs -f buildconfig/k8s-simple-api
```

---

## Cleanup
```shell
$> oc delete all,is,dc,secrets,cm,pvc -l app=k8s-simple-api
```