[source-to-image](https://github.com/openshift/source-to-image)

```shell
$> oc new-app https://gitlab.com/mt-data-group/k8s-simple-api \
    --context-dir=simple-api-s2i \
    --strategy=source \
    -e APP_FILE=main.py

# using a custom image and s2i (run/assemble scripts)
$> oc new-app python:3.9~https://gitlab.com/mt-data-group/k8s-simple-api --strategy=source
```

```text
Now using project "simple-api" on server "https://api.crc.testing:6443".

You can add applications to this project with the 'new-app' command. For example, try:

    oc new-app rails-postgresql-example

to build a new example application in Ruby. Or use kubectl to deploy a simple Kubernetes application:

    kubectl create deployment hello-node --image=k8s.gcr.io/e2e-test-images/agnhost:2.33 -- /agnhost serve-hostname

[mastella@odie OpenShiftProjects]$ oc new-app https://gitlab.com/mt-data-group/k8s-simple-api --context-dir=simple-api-s2i --strategy=source
--> Found image c487d42 (7 weeks old) in image stream "openshift/python" under tag "3.9-ubi8" for "python"

    Python 3.9 
    ---------- 
    Python 3.9 available as container is a base platform for building and running various Python 3.9 applications and frameworks. Python is an easy to learn, powerful programming language. It has efficient high-level data structures and a simple but effective approach to object-oriented programming. Python's elegant syntax and dynamic typing, together with its interpreted nature, make it an ideal language for scripting and rapid application development in many areas on most platforms.

    Tags: builder, python, python39, python-39, rh-python39

    * The source repository appears to match: python
    * A source build using source code from https://gitlab.com/mt-data-group/k8s-simple-api will be created
      * The resulting image will be pushed to image stream tag "k8s-simple-api:latest"
      * Use 'oc start-build' to trigger a new build

--> Creating resources ...
    imagestream.image.openshift.io "k8s-simple-api" created
    buildconfig.build.openshift.io "k8s-simple-api" created
    deployment.apps "k8s-simple-api" created
    service "k8s-simple-api" created
--> Success
    Build scheduled, use 'oc logs -f buildconfig/k8s-simple-api' to track its progress.
    Application is not exposed. You can expose services to the outside world by executing one or more of the commands below:
     'oc expose service/k8s-simple-api' 
    Run 'oc status' to view your app.

```

## Expose the service
```shell

$> # expose the service
$> oc expose svc/k8s-simple-api --port=8080

$> # get the route
$> oc get routes

$> oc get route k8s-simple-api
```

---

## make a change -> checkin -> rebuild
```shell
$> # get all build configs
$> oc get bc

$> # kick-off a new build
$> oc start-build bc/k8s-simple-api

$> # stream logs 
$> oc logs -f bc/k8s-simple-api
```

---

## Cleanup
```shell
$> oc delete all,is,dc,secrets,cm,pvc -l app=k8s-simple-api
```